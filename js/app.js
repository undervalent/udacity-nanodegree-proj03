// Set up globals for enemy, player and powerups
var GLOBALS = {
    enemyCollision  : false,
    powerUpCollision: false,
    enemy           : {
        enemyCount    : Math.floor((Math.random() + 1) * 3), //Set the enemy count
        enemyStartYPos: [225, 141, 61], //Gives the enemy three Y starting options  that correspond to the brick road

        //Randomly generates the enemy speed
        enemySpeed: function () {
            return Math.floor(Math.random() * (300 - 50) + 50);
        }
    },
    player : {
        /*
         Gives the player a random X position that fits inside the width of
         the canvas minus the player image width.
         */
        playerStartX: function () {
            return Math.floor(Math.random() * (505 - 101 - 20 ) + 20);
        },
        /*
         Gives the player a random Y position that positions the player inside the
         green field
         */
        playerStartY : function () {
            return Math.floor(Math.random() * (566 - 330 - 171) + 330);
        },
        playerSpeed : 100
    },
    powerup : {
        startYPosOptions : [240, 141, 61],
        //Gives the powerup a random x pos
        startX : function () {
            return Math.floor(Math.random() * (505 - 101 - 20 ) + 20);
        },
        /*
         Gives the powerup a random Y position that positions the player inside the
         rock field;
         */
        startY : function () {
            return GLOBALS.powerup.startYPosOptions[Math.floor(Math.random() * 3)];
        }
    },
    collisionTest : function (entity1, entity2, colVar) {
        //Check to see if dimensions of the enemy run into the dimensions of the player
        if (entity1.x < (entity2.x + entity2.width) && (entity1.x + entity1.width) > entity2.x && entity1.y < (entity2.y + entity2.height) && (entity1.height + entity1.y) > entity2.y) {
            GLOBALS[colVar] = true;
        }
    }

};

var Enemy = function (x, y, speed) {
    //Enemy variables
    this.speed = speed;
    this.x = x;
    this.y = y;

    //Enemy srite
    this.sprite = 'images/enemy-bug.png';

    //Enemy Sprite dimensions
    this.width = 70;
    this.height = 60;
};

// Update the enemy's position, required method for game
Enemy.prototype.update = function (dt) {
    //Resets the enemy if they reach the end of the board.
    if (this.x > 506) {
        this.x = -100;
        this.y = GLOBALS.enemy.enemyStartYPos[Math.floor(Math.random() * 3)];
        this.speed = GLOBALS.enemy.enemySpeed();
    }
    //Sets the x pos of enemy after each update
    this.x += dt * this.speed;
    //Starts the game over when there is a collision
    if (GLOBALS.enemyCollision) {
        GLOBALS.enemyCollision = false;
        player.reset();
    }
    //Set collision test to see if player collides with enemy
    GLOBALS.collisionTest(player, this, "enemyCollision");
};

// Draw the enemy on the screen, required method for game
Enemy.prototype.render = function () {
    ctx.drawImage(Resources.get(this.sprite), this.x, this.y);
};

// Now write your own player class
// This class requires an update(), render() and
// a handleInput() method.
var Player = function (x, y) {
    this.sprite = 'images/char-boy.png';
    //Sprite positioning vars
    this.x = x;
    this.y = y;
    this.speed = GLOBALS.player.playerSpeed;

    //Player Sprite dimensions
    this.width = 60;
    this.height = 60;
};

Player.prototype.update = function () {
    //Setting boundaries for player
    if(this.x < 0) {
        this.x = 1;
    }
    if (this.x + this.width > 500) {
        this.x = 480 - this.width;
    }
    if(this.y + this.height > 497) {
        this.y = 400;
    }
    //Runs check to see if the player has reached the water
    this.isCompleted();
};

//Render the player
Player.prototype.render = function () {
    ctx.drawImage(Resources.get(this.sprite), this.x, this.y);
};
Player.prototype.reset = function(){
        this.x = GLOBALS.player.playerStartX();
        this.y = GLOBALS.player.playerStartY();
        this.speed = GLOBALS.player.playerSpeed;
};
//Checks to see if the player has reached the water
Player.prototype.isCompleted = function () {
    if (this.y < 0) {
        this.reset();
    }
};
Player.prototype.handleInput = function (key) {
    //Sets the boundaries of when a player can move
    if (key === "up") {
        if (this.y >= 0) {
            this.y -= this.speed;
        }
    }
    if (key === "down") {
        if (this.y <= 395) {
            this.y += this.speed;
        }
    }
    if (key === "right") {
        if (this.x <= 392) {
            this.x += this.speed;
        }
    }
    if (key === "left") {
        if (this.x >= 29) {
            this.x -= this.speed;
        }
    }
};

var Powerup = function (x, y) {
    this.sprite = 'images/star.png';
    //power up position
    this.x = x;
    this.y = y;
    //Power up dimensions
    this.width = 70;
    this.height = 70;
};
Powerup.prototype.update = function () {
    //Handles when a powerup Collision happens
    GLOBALS.collisionTest(player, this, "powerUpCollision");
    if (GLOBALS.powerUpCollision) {
        GLOBALS.powerUpCollision = false;
        this.x = -200;
        player.speed = player.speed * 1.2;
        //Adds Powerboost to player for 1 seconds
        setTimeout(function () {
            player.speed = GLOBALS.player.playerSpeed;
        }, 1000);
        //Reloads powerboost on anvas after 5 seconds
        setTimeout(function () {
            powerup.x = GLOBALS.powerup.startX();
            powerup.y = GLOBALS.powerup.startY();
        }, 5000)
    }
};
Powerup.prototype.render = function () {
    ctx.drawImage(Resources.get(this.sprite), this.x, this.y);
};

// Now instantiate your objects.
// Place all enemy objects in an array called allEnemies
var allEnemies = [];
for (var i = 0; i < GLOBALS.enemy.enemyCount; i++) {
    //Call the globals enemySpeed function to get a random speed
    var speed = GLOBALS.enemy.enemySpeed();
    //Call the Globals enemyStartYPos and pick one of three options.
    var YPosition = GLOBALS.enemy.enemyStartYPos[Math.floor(Math.random() * 3)];
    //Creates a new enemy
    var newEnemy = new Enemy(-200, YPosition, speed);
    //Adds new enemy to the allEnemies Array
    allEnemies.push(newEnemy);
}
var powerup = new Powerup(GLOBALS.powerup.startX(), GLOBALS.powerup.startY());

// Place the player object in a variable called player
var player = new Player(GLOBALS.player.playerStartX(), GLOBALS.player.playerStartY());

// This listens for key presses and sends the keys to your
// Player.handleInput() method. You don't need to modify this.
document.addEventListener('keyup', function (e) {
    var allowedKeys = {
        37: 'left',
        38: 'up',
        39: 'right',
        40: 'down'
    };

    player.handleInput(allowedKeys[e.keyCode]);
});
