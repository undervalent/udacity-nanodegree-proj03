frontend-nanodegree-arcade-game
===============================

To play the game, open the index file in your favorite browser.
Use the up, down, left, right arrows to move your character into the water.
If you collect the star, you get a speed bonus for a short period of time.
Becareful, if a bug hits you, you have to start over.
